
#ifndef __HANDLES_QFJQIWFJEOQWF__
#define __HANDLES_QFJQIWFJEOQWF__

#include <gst/gst.h>

struct AppData {
  GMainLoop *loop;
};

gboolean bus_play_handle(GstBus *bus, GstMessage *msg, gpointer data);
gboolean bus_dtmf_detector_handle(GstBus *bus, GstMessage *msg, gpointer data);

#endif  //__HANDLES_QFJQIWFJEOQWF__