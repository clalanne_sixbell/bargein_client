

#include "handles.hpp"

#include <iostream>

gboolean bus_play_handle(GstBus *bus, GstMessage *msg, gpointer data) {
  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_EOS: {
      auto appData = static_cast<AppData *>(data);
      std::cout << "[bus_play_handle]End of stream closing loop["
                << appData->loop << "]" << '\n';
      g_main_loop_quit(appData->loop);
      break;
    }

    case GST_MESSAGE_ERROR: {
      gchar *debug;
      GError *error;

      gst_message_parse_error(msg, &error, &debug);
      g_free(debug);

      std::cerr << "[bus_play_handle]Error: " << error->message << '\n';
      g_free(error);
      auto *appData = static_cast<AppData *>(data);
      g_main_loop_quit(appData->loop);

      break;
    }
  }
}

gboolean bus_dtmf_detector_handle(GstBus *bus, GstMessage *msg, gpointer data) {
  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_EOS: {
      auto appData = static_cast<AppData *>(data);
      std::cout << "[bus_dtmf_detector_handle]End of stream closing loop["
                << appData->loop << "]" << '\n';
      g_main_loop_quit(appData->loop);
      break;
    }

    case GST_MESSAGE_ERROR: {
      gchar *debug;
      GError *error;

      gst_message_parse_error(msg, &error, &debug);
      g_free(debug);

      std::cerr << "[bus_dtmf_detector_handle]Error: " << error->message
                << '\n';
      g_free(error);
      auto *appData = static_cast<AppData *>(data);
      g_main_loop_quit(appData->loop);

      break;
    }

    case GST_MESSAGE_ELEMENT: {
      const GstStructure *s = gst_message_get_structure(msg);

      std::cout << "[bus_dtmf_detector_handle]  GST_MESSAGE_ELEMENT ["
                << gst_structure_get_name(s) << "]" << '\n';

      if (!strcmp(gst_structure_get_name(s), "dtmf-event")) {
        int number = -1;
        gst_structure_get_int(s, "number", &number);

        std::cout << "[bus_dtmf_detector_handle][GST_MESSAGE_ELEMENT]"
                     "number["
                  << number << "]" << '\n';
      }
    }
  }
}
