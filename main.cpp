
#include "handles.hpp"

#include <gst/gst.h>

#include <iostream>

const int PORT = 6010;
const std::string IP = "192.168.202.122";

void build_play_pipeline(GstElement* pipelinePlay, GstElement* sourcePlay,
                         GstElement* parserPlay, GstElement* encoderPlay,
                         GstElement* rtppayPlay, GstElement* udpsinkPlay) {
  g_object_set(G_OBJECT(sourcePlay), "location", "bienvenidoTmp.wav", nullptr);

  gst_bin_add(GST_BIN(pipelinePlay), sourcePlay);
  gst_bin_add(GST_BIN(pipelinePlay), parserPlay);
  gst_bin_add(GST_BIN(pipelinePlay), encoderPlay);
  gst_bin_add(GST_BIN(pipelinePlay), rtppayPlay);
  gst_bin_add(GST_BIN(pipelinePlay), udpsinkPlay);

  gst_element_link(sourcePlay, parserPlay);
  gst_element_link(parserPlay, encoderPlay);
  gst_element_link(encoderPlay, rtppayPlay);
  gst_element_link(rtppayPlay, udpsinkPlay);
}

void build_rcv_play_pipeline(GstElement* pipelineRCVPlay,
                             GstElement* sourceRCVPlay,
                             GstElement* depayloaderRCVPlay,
                             GstElement* rtpdecoderRCVPlay,
                             GstElement* audiosinkRCVPlay) {
  gst_bin_add(GST_BIN(pipelineRCVPlay), sourceRCVPlay);
  gst_bin_add(GST_BIN(pipelineRCVPlay), depayloaderRCVPlay);
  gst_bin_add(GST_BIN(pipelineRCVPlay), rtpdecoderRCVPlay);
  gst_bin_add(GST_BIN(pipelineRCVPlay), audiosinkRCVPlay);

  gst_element_link(sourceRCVPlay, depayloaderRCVPlay);
  gst_element_link(depayloaderRCVPlay, rtpdecoderRCVPlay);
  gst_element_link(rtpdecoderRCVPlay, audiosinkRCVPlay);
}

int main() {
  std::cout << "main program starting...." << '\n';
  gst_init(nullptr, nullptr);

  /*std::cout << "Building play pipeline ......." << '\n';

  auto loopPlay = g_main_loop_new(nullptr, false);
  auto pipelinePlay = gst_pipeline_new("play_file");

  auto sourcePlay = gst_element_factory_make("filesrc", "file-source");
  auto parserPlay = gst_element_factory_make("wavparse", "parser");
  auto encoderPlay = gst_element_factory_make("mulawenc", "encoder");
  auto rtppayPlay = gst_element_factory_make("rtppcmupay", "rtppay");
  auto udpsinkPlay = gst_element_factory_make("udpsink", "udpsink");

  if (pipelinePlay == nullptr || sourcePlay == nullptr ||
      parserPlay == nullptr || encoderPlay == nullptr ||
      rtppayPlay == nullptr || udpsinkPlay == nullptr) {
    std::cerr << "One element could not be created. Exiting" << '\n';
  }

  build_play_pipeline(pipelinePlay, sourcePlay, parserPlay, encoderPlay,
                      rtppayPlay, udpsinkPlay);

  gst_element_set_state(pipelinePlay, GST_STATE_PAUSED);
  g_object_set(G_OBJECT(udpsinkPlay), "host", IP.c_str(), nullptr);
  g_object_set(G_OBJECT(udpsinkPlay), "port", PORT, nullptr);*/

  std::cout << "Building receiving audio pipeline ......." << '\n';

  auto loopRCVPlay = g_main_loop_new(nullptr, false);
  auto pipelineRCVPlay = gst_pipeline_new("rcv_play");

  auto sourceRCVPlay = gst_element_factory_make("udpsrc", "udpsrc");
  auto depayloaderRCVPlay = gst_element_factory_make("rtppcmudepay", "depay");
  auto rtpdecoderRCVPlay = gst_element_factory_make("mulawdec", "decoder");
  auto audiosinkRCVPlay = gst_element_factory_make("autoaudiosink", "sink");

  if (!pipelineRCVPlay || !sourceRCVPlay | !depayloaderRCVPlay ||
      !rtpdecoderRCVPlay || !audiosinkRCVPlay) {
    std::cerr << "One element could not be created. Exiting" << '\n';
  }

  build_rcv_play_pipeline(pipelineRCVPlay, sourceRCVPlay, depayloaderRCVPlay,
                          rtpdecoderRCVPlay, audiosinkRCVPlay);

  GstCaps* caps = gst_caps_from_string("application/x-rtp");
  g_object_set(G_OBJECT(sourceRCVPlay), "caps", caps, NULL);
  // g_object_set(G_OBJECT(sourceRCVPlay), "host", IP.c_str(), nullptr);
  std::cout << "[1]" << '\n';
  g_object_set(G_OBJECT(sourceRCVPlay), "address", IP.c_str(), nullptr);
  std::cout << "[1.3]" << '\n';
  g_object_set(G_OBJECT(sourceRCVPlay), "port", PORT, nullptr);
  std::cout << "[1.5]" << '\n';

  std::cout << "End of pipeline building......" << '\n';

  auto busPlay = gst_pipeline_get_bus(GST_PIPELINE(pipelinePlay));
  // auto busRCVPlay = gst_pipeline_get_bus(GST_PIPELINE(pipelineRCVPlay));
  std::cout << "[2]" << '\n';

  AppData data = {
      loopPlay,
  };

  // gst_bus_add_watch(busPlay, bus_play_handle, &data);
  gst_bus_add_watch(busPlay, bus_dtmf_detector_handle, &data);

  // gst_element_set_state(pipelinePlay, GST_STATE_PLAYING);
  gst_element_set_state(pipelinePlay, GST_STATE_PLAYING);

  std::cout << "receiving messages from dtmf pipeline" << '\n';
  g_main_loop_run(loopPlay);

  gst_element_set_state(pipelinePlay, GST_STATE_NULL);
  g_main_loop_unref(loopPlay);
  gst_object_unref(GST_OBJECT(pipelinePlay));
  //  gst_object_unref(busPlay);

  return 0;
}